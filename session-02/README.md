Elm Session 2
=============

Focus on separation parts of application into components.

- `components` - Three step tutorial how to decompose _counters_ example app.
- `elm-component-support` - Example of extra library which helps with component decomposition.
- `starwars` - Nice example app from blog [Learning Elm, part 3](http://lucasmreis.github.io/blog/learning-elm-part-3/).
