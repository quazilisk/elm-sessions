module CounterList3 exposing (..)

import Html exposing (..)
import Html.App exposing (..)
import Html.Events exposing (..)
import Array exposing (..)

import Counter exposing (Model, Msg, init, view, update)

main =
    Html.App.beginnerProgram
        { model = init
        , view = view
        , update = update
        }

-- MODEL
type alias Model =
   Array Counter.Model

init : Model
init =
    Array.fromList 
        [ Counter.init
        , Counter.init
        , Counter.init
        ]


-- UPDATE


type Msg
    = NoOp
    | CounterMsg Int Counter.Msg
    | AddCounter
    | RemoveCounter Int


update : Msg -> Model -> Model
update msg model =
    case msg of
        NoOp ->
            model

        CounterMsg counterIdx counterMsg ->
            replaceCounter model counterIdx counterMsg

        AddCounter ->
            Array.append model (Array.fromList [ Counter.init ])

        RemoveCounter counterIdx ->
            removeCounter model counterIdx

replaceCounter : Model -> Int -> Counter.Msg -> Model
replaceCounter model counterIdx counterMsg =
    let 
        counterModel = Array.get counterIdx model
    in 
        case counterModel of
            Just counter ->
                 Array.set counterIdx (Counter.update counterMsg counter) model
            Nothing ->
                model      
      

removeCounter : Model -> Int -> Model
removeCounter model counterIdx =
    removeFromArray counterIdx model

removeFromList i xs =
    (List.take i xs) ++ (List.drop (i + 1) xs) 

removeFromArray i xs =
    xs |> Array.toList |> removeFromList i |> Array.fromList 


-- VIEW

view : Model -> Html Msg
view model =
    div [] 
        [ div [] 
            [ button [ onClick AddCounter ] [ text "Add Counter" ] 
            ]
        , div []
           [ ul []
               (Array.toList (Array.indexedMap listItem model))
           ] 
        ]

listItem : Int -> Counter.Model -> Html Msg
listItem index counter =
    li [] 
        [ button [ onClick (RemoveCounter index) ] [ text "Remove" ]
        , counterView index counter
        ]

counterView : Int -> Counter.Model -> Html Msg
counterView index counter =
    Html.App.map (CounterMsg index) (Counter.view counter)

