module Counter exposing (..)

import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Json


main =
    Html.App.beginnerProgram
        { model = init
        , view = view
        , update = update
        }

-- MODEL


type alias Counter =
    { name : String
    , count : Int
    , editingName : Bool
    , editingDescription : Bool
    }


type alias Model =
    Counter


init : Model
init =
    { name = "Counter #1"
    , count = 0
    , editingName = False
    , editingDescription = False
    }

-- UPDATE


type Msg
    = StartEditName
    | ChangeCounterName String
    | EndEditName
    | CounterInc
    | CounterDec
    | NoOp


update : Msg -> Model -> Model
update msg model =
    case msg of
        CounterInc ->
            { model | count = model.count + 1 }

        CounterDec ->
            { model | count = model.count - 1 }

        StartEditName ->
            { model | editingName = True }

        EndEditName ->
            { model | editingName = False }

        ChangeCounterName newName ->
            { model | name = newName }

        NoOp ->
            model


view : Model -> Html Msg
view model =
    div [ counterStyle ]
        [ counterNameView model
        , div []
            [ text "Counter: "
            , text (toString model.count)
            , button [ onClick CounterInc ] [ text "Inc" ]
            , button [ onClick CounterDec ] [ text "Dec" ]
            ]
        ]


counterNameView : Model -> Html Msg
counterNameView counter =
    if counter.editingName then
        div []
            [ input
                [ value counter.name
                , autofocus True
                , onInput ChangeCounterName
                , onEnter EndEditName
                ]
                []
            , button [ onClick EndEditName ] [ text "Done" ]
            ]
    else
        div [ onClick StartEditName ]
            [ text counter.name
            ]


onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        tagger code =
            if code == 13 then
                msg
            else
                NoOp
    in
        on "keydown" (Json.map tagger keyCode)

counterStyle =
    style
    [ ("backgroundColor", "#eeeeaa")
    , ("height", "60px")
    , ("width", "300px")
    , ("padding", "10px")
    , ("margin", "10px")
    ]
