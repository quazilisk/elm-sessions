port module CounterList exposing (..)


import Html exposing (..)
import Html.App exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Json


main =
   Html.App.beginnerProgram
        { model = model
        , view = view
        , update = update
        }


-- MODEL


type alias Counter =
    { id : Int
    , name : String
    , count : Int
    , editingName : Bool
    , editingDescription : Bool
    }


type alias Model =
    List Counter


model : Model
model =
    []


defaultCounter : Counter
defaultCounter =
    { id = 0
    , name = "Counter"
    , count = 0
    , editingName = False
    , editingDescription = False
    }



-- UPDATE


type Msg
    = Add
    | Remove Int
    | StartEditName Int
    | ChangeCounterName Int String
    | EndEditName Int
    | CounterInc Int
    | CounterDec Int
    | NoOp


update : Msg -> Model -> Model
update msg model =
    case msg of
        Add ->
            List.append model [ newCounter model ]

        Remove idx ->
            removeCounter model idx

        CounterInc idx ->
            updateCounterValue model idx 1

        CounterDec idx ->
            updateCounterValue model idx -1

        StartEditName idx ->
            updateCounterEditNameStatus model idx True

        EndEditName idx ->
            updateCounterEditNameStatus model idx False

        ChangeCounterName idx newName ->
            updateCounterName model idx newName

        NoOp ->
            model

removeCounter : Model -> Int -> Model
removeCounter model id =
    List.filter (\e -> e.id /= id) model


updateCounterName : Model -> Int -> String -> Model
updateCounterName model id newName =
    let
        changeName counter =
            if counter.id == id then
                { counter | name = newName }
            else
                counter
    in
        List.map changeName model


updateCounterEditNameStatus : Model -> Int -> Bool -> Model
updateCounterEditNameStatus model id editing =
    let
        changeStatus counter =
            if counter.id == id then
                { counter | editingName = editing }
            else
                counter
    in
        List.map changeStatus model


updateCounterValue : Model -> Int -> Int -> Model
updateCounterValue model id inc =
    let
        updateCount counter =
            if counter.id == id then
                { counter | count = counter.count + inc }
            else
                counter
    in
        List.map updateCount model


newCounter : Model -> Counter
newCounter model =
    { defaultCounter
        | id = (List.length model)
        , name = "Counter " ++ (toString (List.length model))
    }


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick Add ] [ text "Add Counter" ]
        , div []
            [ ul []
                (List.map counterListItem model)
            ]
        ]


counterListItem : Counter -> Html Msg
counterListItem counter =
    li []
        [ button [ onClick (Remove counter.id) ] [ text "Remove" ]
        , div [ counterStyle ]
            [ div []
                [ (text (toString counter.id))
                ]
            , counterName counter
            , div []
                [ text "Counter: "
                , text (toString counter.count)
                , button [ onClick (CounterInc counter.id) ] [ text "Inc" ]
                , button [ onClick (CounterDec counter.id) ] [ text "Dec" ]
                ]
            ]
        ]


counterName : Counter -> Html Msg
counterName counter =
    if counter.editingName then
        div []
            [ input
                [ value counter.name
                , autofocus True
                , onInput (ChangeCounterName counter.id)
                , onEnter (EndEditName counter.id)
                ]
                []
            , button [ onClick (EndEditName counter.id) ] [ text "Done" ]
            ]
    else
        div [ onClick (StartEditName counter.id) ]
            [ text counter.name
            ]


onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        tagger code =
            if code == 13 then
                msg
            else
                NoOp
    in
        on "keydown" (Json.map tagger keyCode)

counterStyle =
    style
    [ ("backgroundColor", "#eeeeaa")
    , ("height", "60px")
    , ("width", "300px")
    , ("padding", "10px")
    , ("margin", "10px")
    ]