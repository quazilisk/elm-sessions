module CounterList2 exposing (..)

import Html exposing (..)
import Html.App exposing (..)
import Counter exposing (Model, Msg, init, view, update)

main =
    Html.App.beginnerProgram
        { model = init Counter.init Counter.init
        , view = view
        , update = update
        }

type alias Model =
    { firstCounter : Counter.Model
    , secondCounter : Counter.Model
    }


init : Counter.Model -> Counter.Model -> Model
init first second =
    { firstCounter = first
    , secondCounter = second
    }

-- UPDATE


type Msg
    = First Counter.Msg
    | Second Counter.Msg


update : Msg -> Model -> Model
update msg model =
    case msg of
        First m ->
            { model | firstCounter = Counter.update m model.firstCounter }

        Second m ->
            { model | secondCounter = Counter.update m model.secondCounter }


-- VIEW


view : Model -> Html Msg
view model =
    div
        []
        [ div []
            [ Html.App.map First (Counter.view model.firstCounter)
            ]
        , div []
            [ Html.App.map Second (Counter.view model.secondCounter)
            ]
        ]
