Components Example
==================

Components Example Describes how to decompose application into components.

- `src/CounterList.elm` Whole application in one file/module.
- `src/CounterList2.elm` and `Counter` Counter as separated component.
- `src/CounterList3.elm` and `Counter` Counter ss separated component + variable number of counters.   