# Demo of Elm Hot Loader with Time-Travel Debugger

## Hot Loader
- Project page: https://github.com/jinjor/elm-time-travel
- Dependency
```
"jinjor/elm-time-travel": "1.0.17 <= v < 2.0.0"
```
- Change program launcher
```
import TimeTravel.Html.App as TimeTravel
..
main : Program Never
main =
    TimeTravel.program
```

## Elm Hot Loader Starter Kit
- Project page: https://github.com/fluxxu/elm-hot-loader

### Requirements
* Elm 0.17+
* elm-webpack-loader 3.0.0+
* elm-hot-loader 0.3.0+

### Setup
```shell
# clone this repository
git clone https://github.com/fluxxu/elm-hot-loader-starter.git
cd elm-hot-loader-starter
# install node dependencies
npm install
```

### Development
```shell
npm run dev
```
open http://localhost:3000

### Build for production
```shell
# build project, copy js, html and assets to ./dist
npm run build
```
