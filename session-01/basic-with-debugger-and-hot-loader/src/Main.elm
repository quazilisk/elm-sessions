port module Main exposing (..)

import Html exposing (div, button, text, h2, input, ul, li)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (..)
import TimeTravel.Html.App as TimeTravel
import Time

main : Program Never
main =
    TimeTravel.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }


-- MODEL

type alias Model = 
  { count : Int
  }

init : (Model, Cmd msg)
init =
  (Model 0, Cmd.none) 


-- UPDATE


type Msg = Increment | Decrement | IncrementIfOdd | Tick

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Increment ->
      ({ model | count = model.count + 1000 }, Cmd.none)

    Decrement ->
      ({ model | count = model.count - 1 }, Cmd.none)

    IncrementIfOdd ->
      if model.count % 2 /= 0 then
        ({ model | count = model.count + 1 }, Cmd.none)
      else
        (model, Cmd.none)

    Tick ->
        ({ model | count = model.count + 1}, Cmd.none)


-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.batch
    [ Time.every Time.second <| always Tick
    ]

-- VIEW

view : Model -> Html.Html Msg
view model =
  div []
    [ div [ style boxStyle ]
        [ Html.h1 [] [ text "Elm Counter Example !!!" ]
        , button [ onClick Decrement ] [ text "-" ]
        , div [] [ text (toString model.count) ]
        , button [ onClick Increment ] [ text "+" ]
        , button [ onClick IncrementIfOdd ] [ text "Incremet if odd" ]
        ]
    ]

boxStyle : List ( String, String )
boxStyle =
  [ ("border", "1px solid #ccc")
  , ("border-radius", "20px")
  , ("padding", "10px")
  , ("margin", "10px")
  ]