Build
-----

```
elm-make src/Maim.elm
open index.html
```

or just run Reactor:

```
elm-reactor
```
