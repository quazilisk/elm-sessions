module Main exposing (..)

import Html exposing (..)
import Html.App exposing (..)
import Html.Events exposing (..)

main =
  Html.App.beginnerProgram 
    { model = model
    , view = view
    , update = update
    }  
  
-- MODEL

model = 
  1
  
-- UPDATE

type Msg 
  = Add
  | Remove

update : Msg -> Int -> Int
update msg model =
  case msg of
    Add ->
      model + 1
    Remove ->
      model - 1

-- VIEW
view : Int -> Html Msg
view model =
  div []
    [ div []
      [ text (toString model) 
      , button [ onClick Add ] [ text "ADD" ]
      , button [ onClick Remove ] [ text "REMOVE" ]
      ]
    ]
