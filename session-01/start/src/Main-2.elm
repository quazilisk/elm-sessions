module Main exposing (..)

import Html exposing (..)
import Html.App exposing (..)
import Html.Events exposing (..)

main =
  Html.App.beginnerProgram 
    { model = init
    , view = view
    , update = update
    }  
  

-- MODEL

type alias Model =
  { title : String
  , counter : Int 
  , extra : String
  }


init : Model
init = 
  { title = ""
  , counter = 0
  , extra = "bala" 
  }

-- UPDATE

type Msg 
  = Add
  | Remove

update : Msg -> Model -> Model
update msg model =
  case msg of
    Add ->
      { model | counter = model.counter + 1 }
    Remove ->
      { model | counter = model.counter - 1 }

-- VIEW
view : Model -> Html Msg
view model =
  div []
    [ div []
      [ text (toString model.counter)  
      , text model.title
      , button [ onClick Add ] [ text "ADD" ]
      , button [ onClick Remove ] [ text "REMOVE" ]
      ]
    , div [] 
      [ text (toString model) ]
    ]
