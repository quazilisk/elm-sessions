module Main exposing (..)

import Html exposing (..)
import Html.App exposing (..)
import Html.Events exposing (..)

main =
  Html.App.beginnerProgram 
    { model = init
    , view = view
    , update = update
    }  
  

-- MODEL


type alias Person = 
  { name : String
  , age : Int
  , id : Int
  }

type alias Model =
  List Person


init : Model
init = 
  []

-- UPDATE

type Msg 
  = Add
  | Remove Int  

update : Msg -> Model -> Model
update msg model =
  case msg of
    
    Add ->
      List.append model [ newPersonWithId (List.length model) ]

    Remove id ->
      let 
        comparePersonId person =
          person.id /= id 
      in 
        List.filter comparePersonId model


newPersonWithId id =
  Person "Alexander" 33 id  

-- VIEW
view : Model -> Html Msg
view model =
  div []
    [ div []
      [ button [ onClick Add ] [ text "ADD" ]      
      ]
    , div [] 
      [ ul [] 
        (List.map createItem model)  
      ]
    , div [] 
      [ text (toString model) ]
    ]

createItem : Person -> Html Msg
createItem person =
  li [] 
    [ text person.name
    , button [ onClick (Remove person.id) ] [ text "REMOVE" ]  
    ]
